#include <hyprland/src/Compositor.hpp>
#include <hyprland/src/debug/Log.hpp>
#include <hyprland/src/includes.hpp>
#include <hyprland/src/managers/LayoutManager.hpp>
#include <hyprland/src/plugins/PluginAPI.hpp>
#include <optional>

#include "Slider.hpp"
#include "globals.hpp"
#include "log.hpp"
#include "dispatchers.hpp"

namespace {
    std::optional<Direction> parseMoveArg(std::string arg) {
        if (arg == "l" || arg == "left")
            return Direction::Left;
        if (arg == "r" || arg == "right")
            return Direction::Right;
        if (arg == "u" || arg == "up")
            return Direction::Up;
        if (arg == "d" || arg == "dn" || arg == "down")
            return Direction::Down;
        if (arg == "c" || arg == "center" || arg == "centre")
            return Direction::Center;
        else
            return {};
    }

    int workspace_for_action() {
        if (g_pLayoutManager->getCurrentLayout() != g_SlidrLayout.get())
            return -1;

        int workspace_id = g_pCompositor->m_pLastMonitor->activeWorkspace->m_iID;

        if (workspace_id == -1)
            return -1;
        auto workspace = g_pCompositor->getWorkspaceByID(workspace_id);
        if (workspace == nullptr)
            return -1;
        if (workspace->m_bHasFullscreenWindow)
            return -1;

        return workspace_id;
    }

    void dispatch_cyclesize(std::string arg) {
        Debug::disableStdout = false;
        slidr_log(INFO, "{}", __PRETTY_FUNCTION__);

        auto workspace = workspace_for_action();
        if (workspace == -1)
            return;

        g_SlidrLayout->cycleWindowSize(workspace);
    }

    void dispatch_movefocus(std::string arg) {
        Debug::disableStdout = false;
        slidr_log(INFO, "{} - {}", __PRETTY_FUNCTION__, arg);

        auto workspace = workspace_for_action();
        if (workspace == -1)
            return;

        auto args = CVarList(arg);
        if (auto direction = parseMoveArg(args[0])) {
            g_SlidrLayout->moveFocus(workspace, *direction);
        }
    }

    void dispatch_alignwindow(std::string arg) {
        Debug::disableStdout = false;
        slidr_log(INFO, "{} - {}", __PRETTY_FUNCTION__, arg);

        auto workspace = workspace_for_action();
        if (workspace == -1)
            return;

        auto args = CVarList(arg);
        if (auto direction = parseMoveArg(args[0])) {
            g_SlidrLayout->alignWindow(workspace, *direction);
        }
    }

    void dispatch_admitwindow(std::string arg) {
        Debug::disableStdout = false;
        slidr_log(INFO, "{} - {}", __PRETTY_FUNCTION__, arg);

        auto workspace = workspace_for_action();
        if (workspace == -1)
            return;

        g_SlidrLayout->admitWindowLeft(workspace);
    }

    void dispatch_expelwindow(std::string arg) {
        Debug::disableStdout = false;
        slidr_log(INFO, "{} - {}", __PRETTY_FUNCTION__, arg);

        auto workspace = workspace_for_action();
        if (workspace == -1)
            return;

        g_SlidrLayout->expelWindowRight(workspace);
    }

    void dispatch_printsliders(std::string arg) {
        slidr_log(INFO, "{} - {}", __PRETTY_FUNCTION__, arg);

        g_SlidrLayout->printLayout();
    }
}

void dispatchers::addDispatchers() {
    HyprlandAPI::addDispatcher(PHANDLE, "slidr:cyclesize", dispatch_cyclesize);
    HyprlandAPI::addDispatcher(PHANDLE, "slidr:movefocus", dispatch_movefocus);
    HyprlandAPI::addDispatcher(PHANDLE, "slidr:alignwindow", dispatch_alignwindow);
    HyprlandAPI::addDispatcher(PHANDLE, "slidr:admitwindow", dispatch_admitwindow);
    HyprlandAPI::addDispatcher(PHANDLE, "slidr:expelwindow", dispatch_expelwindow);
    HyprlandAPI::addDispatcher(PHANDLE, "slidr:printsliders", dispatch_printsliders);
}
