#include <algorithm>

#include <cstddef>
#include <deque>
#include <hyprland/src/Compositor.hpp>
#include <hyprland/src/helpers/Vector2D.hpp>

#include "globals.hpp"
#include "log.hpp"
#include "Slider.hpp"

WinStack::WinStack(PHLWINDOW w) : width(StackWidth::OneThird), act_idx(0), act_maxd(false) {
    stack.push_back(w);
}

bool WinStack::isActive(PHLWINDOW w) {
    return stack[act_idx] == w;
}

bool WinStack::hasWindow(PHLWINDOW w) {
    for (auto sw : stack) {
        if (sw == w)
            return true;
    }
    return false;
}

bool WinStack::empty() {
    return stack.empty();
}

size_t WinStack::size() {
    return stack.size();
}

void WinStack::removeWindow(PHLWINDOW w) {
    auto it = std::find(stack.begin(), stack.end(), w);
    if (it != stack.end() && it - stack.begin() >= 0) {
        act_idx = std::max(0, act_idx - 1);
    }
    std::erase(stack, w);
}

void WinStack::focusWindow(PHLWINDOW w) {
    auto it = std::find(stack.begin(), stack.end(), w);
    if (it != stack.end()) {
        act_idx = it - stack.begin();
    }
}

bool WinStack::toggleFullscreenActiveWindow() {
    stack[act_idx]->m_bIsFullscreen = !stack[act_idx]->m_bIsFullscreen;
    return stack[act_idx]->m_bIsFullscreen;
}

bool WinStack::isFullscreenActiveWindow() {
    return stack[act_idx]->m_bIsFullscreen;
}

PHLWINDOW WinStack::getActiveWindow() {
    return stack[act_idx];
}

void WinStack::admitWindow(PHLWINDOW w) {
    stack.push_back(w);
    act_idx = stack.size() - 1;
}

PHLWINDOW WinStack::expelActive() {
    slidr_log(INFO, "{}", __PRETTY_FUNCTION__);

    auto w = stack[act_idx];
    act_idx = std::max(0, act_idx - 1);
    std::erase(stack, w);
    return w;
}

void WinStack::moveFocusUp() {
    act_idx = std::max(0, act_idx - 1);
}

void WinStack::moveFocusDown() {
    act_idx = std::min(static_cast<int>(stack.size()) - 1, act_idx + 1);
}

void WinStack::moveActiveUp() {
    if (act_idx == 0)
        return;

    std::swap(stack[act_idx], stack[act_idx - 1]);
    act_idx--;
};
void WinStack::moveActiveDown() {
    if (act_idx == stack.size() - 1)
        return;

    std::swap(stack[act_idx], stack[act_idx + 1]);
    act_idx++;
};

void WinStack::applyGeom() {
    if (isFullscreenActiveWindow()) {
        stack[act_idx]->m_vPosition = Vector2D(geom.x, geom.y);
        stack[act_idx]->m_vSize = Vector2D(geom.w, geom.h);
        stack[act_idx]->m_vRealPosition = stack[act_idx]->m_vPosition;
        stack[act_idx]->m_vRealSize = stack[act_idx]->m_vSize;
    } else {
        auto wh = geom.h / stack.size();
        auto ty = geom.y;
        for (auto w : stack) {
            w->m_vPosition = Vector2D(geom.x, ty);
            w->m_vSize = Vector2D(geom.w, wh);
            w->m_vRealPosition = w->m_vPosition;
            w->m_vRealSize = w->m_vSize;
            ty += wh;
        }
    }
}

bool Slider::isActive(PHLWINDOW w) {
    return active && active->isActive(w);
}

bool Slider::hasWindow(PHLWINDOW w) {
    if (active->hasWindow(w))
        return true;

    for (auto lw : lefts) {
        if (lw.hasWindow(w))
            return true;
    }

    for (auto rw : rights) {
        if (rw.hasWindow(w))
            return true;
    }
    return false;
}

bool Slider::hasWorkspace(int ws) {
    return workspace == ws;
}

void Slider::addActiveWindow(PHLWINDOW w) {
    if (active) {
        // assume new window should be visible, so turn off fullscreen first
        if (active->isFullscreenActiveWindow())
            toggleFullscreenActiveWindow();

        auto old_active = *active;

        lefts.push_front(*active);
        active = WinStack(w);
        active->geom.setPos(old_active.geom.x + old_active.geom.w, maxsz.y);
    } else {
        active = WinStack(w);
    }

    recalculateSizePos();
}

void Slider::removeWindow(PHLWINDOW w) {
    if (active->hasWindow(w)) {
        active->removeWindow(w);
        if (active->empty()) {
            if (!lefts.empty()) {
                active = lefts.front();
                lefts.pop_front();
            } else if (!rights.empty()) {
                active = rights.front();
                rights.pop_front();
            } else {
                active.reset();
            }
        }
    } else {
        lefts.remove_if([w](auto& l) { return l.hasWindow(w); });
        rights.remove_if([w](auto& r) { return r.hasWindow(w); });
    }

    recalculateSizePos();
}

void Slider::focusWindow(PHLWINDOW w) {
    if (active->hasWindow(w)) {
        active->focusWindow(w);
        return;
    }

    auto it = std::find_if(lefts.begin(), lefts.end(), [w](auto l) { return l.hasWindow(w); });
    if (it != lefts.end()) {
        while (!active->hasWindow(w)) {
            moveFocusLeft();
        }
        active->focusWindow(w);
    } else {
        it = std::find_if(rights.begin(), rights.end(), [w](auto r) { return r.hasWindow(w); });
        if (it != rights.end()) {
            while (!active->hasWindow(w)) {
                moveFocusRight();
            }
            active->focusWindow(w);
        }
    }

    recalculateSizePos();
}

void Slider::resizeActiveStack() {
    if (!active || active->act_maxd)
        return;

    switch (active->width) {
        case StackWidth::OneThird: active->width = StackWidth::OneHalf; break;
        case StackWidth::OneHalf: active->width = StackWidth::TwoThirds; break;
        case StackWidth::TwoThirds: active->width = StackWidth::OneThird; break;
    }

    recalculateSizePos();
}

void Slider::centerActiveStack() {
    if (!active || active->act_maxd)
        return;

    switch (active->width) {
        case StackWidth::OneThird: active->geom.setPos(maxsz.x + maxsz.w / 3, maxsz.y); break;
        case StackWidth::OneHalf: active->geom.setPos(maxsz.x + maxsz.w / 4, maxsz.y); break;
        case StackWidth::TwoThirds: active->geom.setPos(maxsz.x + maxsz.w / 6, maxsz.y); break;
    }

    recalculateSizePos();
}

void Slider::toggleMaximizeActiveStack() {
    if (!active)
        return;

    active->act_maxd = !active->act_maxd;
    if (active->act_maxd) {
        active->geom.setSize(maxsz.w, maxsz.h);

        recalculateSizePos();
    } else {
        switch (active->width) {
            case StackWidth::OneThird: active->geom.setSize(maxsz.w / 3, maxsz.h); break;
            case StackWidth::OneHalf: active->geom.setSize(maxsz.w / 2, maxsz.h); break;
            case StackWidth::TwoThirds: active->geom.setSize(2 * maxsz.w / 3, maxsz.h); break;
        }

        centerActiveStack();
    }
}

void Slider::toggleFullscreenActiveWindow() {
    if (!active)
        return;

    const auto PWORKSPACE = g_pCompositor->getWorkspaceByID(active->stack[active->act_idx]->m_pWorkspace->m_iID);

    auto fullscreen = active->toggleFullscreenActiveWindow();
    PWORKSPACE->m_bHasFullscreenWindow = fullscreen;

    if (fullscreen) {
        slidr_log(INFO, "activate fullscreen");
        active->geom = fullsz;

        PWORKSPACE->m_efFullscreenMode = eFullscreenMode::FULLSCREEN_FULL;
        active->applyGeom();
    } else {
        slidr_log(INFO, "de-activate fullscreen");
        active->geom = maxsz;

        centerActiveStack();
    }
}

void Slider::ensureFullscreenActiveWindow() {
    if (!active)
        return;

    active->geom = SBox(fullsz.x, fullsz.y, fullsz.w, fullsz.h);
    active->applyGeom();
}

void Slider::moveActiveStack(Direction dir) {
    switch (dir) {
        case Direction::Right:
            if (rights.size() > 0) {
                auto rs = rights.front();
                lefts.push_front(rs);
                rights.pop_front();

                auto tmp = rs.geom;
                rs.geom.setPos(active->geom.x, active->geom.y);
                active->geom.setPos(tmp.x, tmp.y);
            }
            break;
        case Direction::Left:
            if (lefts.size() > 0) {
                auto ls = lefts.front();
                rights.push_front(ls);
                lefts.pop_front();

                auto tmp = ls.geom;
                ls.geom.setPos(active->geom.x, active->geom.y);
                active->geom.setPos(tmp.x, tmp.y);
            }
            break;
        case Direction::Up: active->moveActiveUp(); break;
        case Direction::Down: active->moveActiveDown(); break;
        case Direction::Center: return;
    }

    recalculateSizePos();
}

void Slider::moveFocus(Direction dir) {
    switch (dir) {
        case Direction::Left: moveFocusLeft(); break;
        case Direction::Right: moveFocusRight(); break;
        case Direction::Up: active->moveFocusUp(); break;
        case Direction::Down: active->moveFocusDown(); break;
        case Direction::Center: return;
    }

    recalculateSizePos();
}

void Slider::alignStick(Direction dir) {
    if (!active || active->act_maxd) // TODO bail early for fullscreen too
        return;

    switch (dir) {
        case Direction::Left: active->geom.setPos(maxsz.x, maxsz.y); break;
        case Direction::Right: active->geom.setPos(maxsz.x + maxsz.w - active->geom.w, maxsz.y); break;
        case Direction::Center: centerActiveStack(); break;
        case Direction::Up:
        case Direction::Down: return;
    }

    recalculateSizePos();
}

void Slider::admitWindowLeft() {
    if (!active || active->act_maxd) // TODO bail early for fullscreen too
        return;
    if (active->size() != 1) // TODO reconsider this
        return;
    if (lefts.size() == 0)
        return;

    auto w = active->getActiveWindow();
    auto ls = lefts.front();
    lefts.pop_front();
    ls.admitWindow(w);
    active = ls;

    recalculateSizePos();
}

void Slider::expelWindowRight() {
    if (!active || active->act_maxd) // TODO bail early for fullscreen too
        return;
    if (active->size() == 1)
        return;

    auto w = active->expelActive();
    auto width = active->width;
    lefts.push_front(*active);
    active = WinStack(w);
    active->width = width;

    recalculateSizePos();
}

void Slider::recalculateSizePos() {
    if (!active)
        return;

    if (active->isFullscreenActiveWindow()) {
        active->geom = fullsz;
        active->applyGeom();
        return;
    }

    // set geom.{w,h} for all windows in all stacks
    switch (active->width) {
        case StackWidth::OneThird: active->geom.setSize(maxsz.w / 3, maxsz.h); break;
        case StackWidth::OneHalf: active->geom.setSize(maxsz.w / 2, maxsz.h); break;
        case StackWidth::TwoThirds: active->geom.setSize(2 * maxsz.w / 3, maxsz.h); break;
    }
    if (active->act_maxd)
        active->geom.setSize(maxsz.w, maxsz.h);
    for (auto ls : lefts) {
        switch (ls.width) {
            case StackWidth::OneThird: ls.geom.setSize(maxsz.w / 3, maxsz.h); break;
            case StackWidth::OneHalf: ls.geom.setSize(maxsz.w / 2, maxsz.h); break;
            case StackWidth::TwoThirds: ls.geom.setSize(2 * maxsz.w / 3, maxsz.h); break;
        }
        if (ls.act_maxd)
            ls.geom.setSize(maxsz.w, maxsz.h);
    }
    for (auto rs : rights) {
        switch (rs.width) {
            case StackWidth::OneThird: rs.geom.setSize(maxsz.w / 3, maxsz.h); break;
            case StackWidth::OneHalf: rs.geom.setSize(maxsz.w / 2, maxsz.h); break;
            case StackWidth::TwoThirds: rs.geom.setSize(2 * maxsz.w / 3, maxsz.h); break;
        }
        if (rs.act_maxd)
            rs.geom.setSize(maxsz.w, maxsz.h);
    }

    // set geom.{x,h} for all windows
    // 1. active visible
    auto lefts_width = std::ranges::fold_left(lefts, 0, [](auto acc, auto ls) { return acc + ls.geom.w; });
    auto rights_width = std::ranges::fold_left(rights, 0, [](auto acc, auto rs) { return acc + rs.geom.w; });

    if (lefts_width + active->geom.w + rights_width < maxsz.w) {
        auto border = (maxsz.w - lefts_width - active->geom.w - rights_width) / 2.;

        active->geom.setPos(maxsz.x + border + lefts_width, maxsz.y);
    } else {
        auto left_edge = active->geom.x - lefts_width - maxsz.x;
        auto right_edge = (maxsz.x + maxsz.w) - (active->geom.x + active->geom.w + rights_width);

        if (active->geom.x < maxsz.x) {
            active->geom.setPos(maxsz.x, maxsz.y);
        } else if (active->geom.x + active->geom.w > maxsz.x + maxsz.w) {
            active->geom.setPos(maxsz.x + maxsz.w - active->geom.w, maxsz.y);
        } else if (left_edge > 0) {
            active->geom.setPos(maxsz.x + lefts_width, maxsz.y);
        } else if (right_edge > 0) {
            active->geom.setPos(maxsz.x + maxsz.w - rights_width - active->geom.w, maxsz.y);
        }
    }
    // 2. adjust positions of windows to the left
    auto ts = *active;
    for (auto& ls : lefts) {
        ls.geom.setPos(ts.geom.x - ls.geom.w, maxsz.y);
        ts = ls;
    }
    // 3. adjust positions of windows to the right
    ts = *active;
    for (auto& rw : rights) {
        rw.geom.setPos(ts.geom.x + ts.geom.w, maxsz.y);
        ts = rw;
    }

    // apply geom
    active->applyGeom();
    for (auto& ls : lefts) {
        ls.applyGeom();
    }
    for (auto& rs : rights) {
        rs.applyGeom();
    }
}

PHLWINDOW Slider::getActiveWindow() {
    if (!active)
        return nullptr;
    else
        return active->getActiveWindow();
}

void Slider::moveFocusLeft() {
    if (lefts.size() == 0)
        return;

    rights.push_front(*active);
    active = lefts.front();
    lefts.pop_front();
}

void Slider::moveFocusRight() {
    if (rights.size() == 0)
        return;

    lefts.push_front(*active);
    active = rights.front();
    rights.pop_front();
}

void dbgPrintSlider(Slider& s) {
    using namespace std;

    cout << format("workspace: {}", s.workspace) << endl;
    cout << format(" fullsz: {}", s.fullsz) << endl;
    cout << format(" maxsz: {}", s.maxsz) << endl;

    if (!s.active) {
        cout << " No active window" << endl;
        return;
    }

    if (s.lefts.size() > 0) {
        cout << " lefts:" << endl;
        for (auto& lws : s.lefts) {
            cout << format("  {}", lws) << endl;
            for (auto w : lws.stack) {
                cout << format("    {} - pos:{} sz:{}", w, w->m_vPosition, w->m_vSize) << endl;
            }
        }
    }

    cout << format(" active: {}", *s.active) << endl;
    for (auto w : s.active->stack) {
        cout << format("  {} - pos:{} sz:{}", w, w->m_vPosition, w->m_vSize) << endl;
    }

    if (s.rights.size() > 0) {
        cout << " rights:" << endl;
        for (auto& rws : s.rights) {
            cout << format("  {}", rws) << endl;
            for (auto w : rws.stack) {
                cout << format("    {} - pos:{} sz:{}", w, w->m_vPosition, w->m_vSize) << endl;
            }
        }
    }
}
