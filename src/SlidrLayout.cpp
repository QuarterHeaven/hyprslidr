#include <algorithm>
#include <hyprland/src/helpers/Vector2D.hpp>
#include <hyprland/src/desktop/Workspace.hpp>
#include <memory>

#include <hyprland/src/Compositor.hpp>
#include <hyprland/src/debug/Log.hpp>

#include "Slider.hpp"
#include "globals.hpp"
#include "log.hpp"
#include "SlidrLayout.hpp"

void SlidrLayout::onWindowCreatedTiling(PHLWINDOW window, eDirection) {
    Debug::disableStdout = false;
    slidr_log(INFO, "{}", __PRETTY_FUNCTION__);

    const auto PMONITOR = g_pCompositor->getMonitorFromID(window->m_iMonitorID);
    const auto SIZE = PMONITOR->vecSize;
    const auto POS = PMONITOR->vecPosition;
    const auto TOPLEFT = PMONITOR->vecReservedTopLeft;
    auto fullsz = SBox(POS, SIZE);
    auto maxsz = SBox(POS.x, POS.y + TOPLEFT.y, SIZE.x, SIZE.y - TOPLEFT.y);

    auto s = getSliderForWorkspace(window->m_pWorkspace->m_iID);
    if (s == nullptr) {
        sliders.push_back(std::make_unique<Slider>(Slider(window->m_pWorkspace->m_iID, window, fullsz, maxsz)));
    } else {
        s->addActiveWindow(window);
    }
}

void SlidrLayout::onWindowRemovedTiling(PHLWINDOW window) {
    Debug::disableStdout = false;
    slidr_log(INFO, "{}", __PRETTY_FUNCTION__);

    auto s = getSliderForWindow(window);
    if (s == nullptr) {
        slidr_log(WARN, "removing unknown window");
        return;
    }
    s->removeWindow(window);
}

void SlidrLayout::onWindowFocusChange(PHLWINDOW window) {
    Debug::disableStdout = false;
    slidr_log(INFO, "{} - w:{}", __PRETTY_FUNCTION__, window);

    if (window == nullptr) { // no window has focus
        return;
    }

    auto s = getSliderForWindow(window);
    if (s == nullptr) {
        slidr_log(WARN, "focusing unknown window");
        return;
    }
    s->focusWindow(window);
}

bool SlidrLayout::isWindowTiled(PHLWINDOW window) {
    return getSliderForWindow(window) != nullptr;
}

void SlidrLayout::recalculateMonitor(const int& monitor_id) {
    Debug::disableStdout = false;
    slidr_log(INFO, "{} - w:{}", __PRETTY_FUNCTION__, monitor_id);

    const auto PMONITOR = g_pCompositor->getMonitorFromID(monitor_id);
    if (!PMONITOR || !PMONITOR->activeWorkspace)
        return;

    g_pHyprRenderer->damageMonitor(PMONITOR); // ??

    const auto PWORKSPACE = PMONITOR->activeWorkspace;
    if (!PWORKSPACE)
        return;

    // TODO
    // handle the "special workspace"? PMONITOR->specialWorkspaceID
    //
    // handle if the workspace has a fullscreen window (only if mode is
    // FULLSCREEN_FULL)! PWORKSPACE->m_bHasFullscreenWindow

    auto s = getSliderForWorkspace(PWORKSPACE->m_iID);
    if (s == nullptr)
        return;

    const auto SIZE = PMONITOR->vecSize;
    const auto POS = PMONITOR->vecPosition;
    const auto TOPLEFT = PMONITOR->vecReservedTopLeft;
    auto fullsz = SBox(POS, SIZE);
    auto maxsz = SBox(POS.x, POS.y + TOPLEFT.y, SIZE.x, SIZE.y - TOPLEFT.y);

    s->setSizes(fullsz, maxsz);
    if (PWORKSPACE->m_bHasFullscreenWindow && PWORKSPACE->m_efFullscreenMode == FULLSCREEN_FULL) {
        s->ensureFullscreenActiveWindow();
    } else {
        s->recalculateSizePos();
    }
}

void SlidrLayout::recalculateWindow(PHLWINDOW window) {
    Debug::disableStdout = false;
    slidr_log(INFO, "{} - w:{}", __PRETTY_FUNCTION__, window);

    auto s = getSliderForWindow(window);
    if (s == nullptr)
        return;

    s->recalculateSizePos();
}

void SlidrLayout::resizeActiveWindow(const Vector2D&, eRectCorner, PHLWINDOW) {}

void SlidrLayout::fullscreenRequestForWindow(PHLWINDOW window, eFullscreenMode fullscreenmode, bool on) {
    Debug::disableStdout = false;
    slidr_log(INFO, "{}", __PRETTY_FUNCTION__);
    slidr_log(INFO, "  {} - {}", fullscreenmode == FULLSCREEN_FULL ? "full" : "max", on);

    auto s = getSliderForWindow(window);
    if (s == nullptr) {
        slidr_log(WARN, "fullscreen for unknown window");
        return;
    } // assuming window is active for now

    const auto PWORKSPACE = g_pCompositor->getWorkspaceByID(window->m_pWorkspace->m_iID);
    switch (fullscreenmode) {
        case eFullscreenMode::FULLSCREEN_FULL:
            if (on == window->m_bIsFullscreen)
                return;

            // if the window wants to be fullscreen but there already is one, ignore the request.
            if (PWORKSPACE->m_bHasFullscreenWindow && on)
                return;

            s->toggleFullscreenActiveWindow();
            break;
        case eFullscreenMode::FULLSCREEN_MAXIMIZED: s->toggleMaximizeActiveStack(); break;
        default: return;
    }
}

std::any SlidrLayout::layoutMessage(SLayoutMessageHeader header, std::string content) {
    return "";
}

SWindowRenderLayoutHints SlidrLayout::requestRenderHints(PHLWINDOW) {
    return {};
}

void SlidrLayout::switchWindows(PHLWINDOW, PHLWINDOW) {}

void SlidrLayout::moveWindowTo(PHLWINDOW window, const std::string& direction, bool) {
    Debug::disableStdout = false;
    slidr_log(INFO, "{}", __PRETTY_FUNCTION__);
    slidr_log(INFO, "   window:{} - direction:{}", window, direction);

    auto s = getSliderForWindow(window);
    if (s == nullptr) {
        slidr_log(WARN, "moving unknown window");
        return;
    } else if (!s->isActive(window)) {
        slidr_log(WARN, "can't move non-active window");
        return;
    }

    switch (direction.at(0)) {
        case 'l': s->moveActiveStack(Direction::Left); break;
        case 'r': s->moveActiveStack(Direction::Right); break;
        case 'u': s->moveActiveStack(Direction::Up); break;
        case 'd': s->moveActiveStack(Direction::Down); break;
        default: slidr_log(WARN, "no support for moving in direction {}", direction); break;
    }
}

void SlidrLayout::alterSplitRatio(PHLWINDOW, float, bool) {}

std::string SlidrLayout::getLayoutName() {
    return "slidr";
}

PHLWINDOW SlidrLayout::getNextWindowCandidate(PHLWINDOW old_window) {
    Debug::disableStdout = false;
    slidr_log(INFO, "{}", __PRETTY_FUNCTION__);
    slidr_log(INFO, "   old_window:{}", old_window);

    int workspace_id = g_pCompositor->m_pLastMonitor->activeWorkspace->m_iID;
    auto s = getSliderForWorkspace(workspace_id);
    if (s == nullptr)
        return nullptr;
    else
        return s->getActiveWindow();
}

void SlidrLayout::replaceWindowDataWith(PHLWINDOW, PHLWINDOW) {}

void SlidrLayout::onEnable() {
    for (auto& window : g_pCompositor->m_vWindows) {
        onWindowCreatedTiling(window);
    }
}

void SlidrLayout::onDisable() {
    sliders.clear();
}

void SlidrLayout::cycleWindowSize(int workspace) {
    Debug::disableStdout = false;
    slidr_log(INFO, "{} - {}", __PRETTY_FUNCTION__, workspace);

    auto s = getSliderForWorkspace(workspace);
    if (s == nullptr) {
        slidr_log(INFO, "No slider on workspace {}", workspace);
        return;
    }

    s->resizeActiveStack();
}

void SlidrLayout::moveFocus(int workspace, Direction direction) {
    auto s = getSliderForWorkspace(workspace);
    if (s == nullptr) {
        slidr_log(INFO, "No slider on workspace {}", workspace);
        return;
    }

    s->moveFocus(direction);
    g_pCompositor->focusWindow(s->getActiveWindow());
}

void SlidrLayout::alignWindow(int workspace, Direction direction) {
    auto s = getSliderForWorkspace(workspace);
    if (s == nullptr) {
        slidr_log(INFO, "No slider on workspace {}", workspace);
        return;
    }

    s->alignStick(direction);
}

void SlidrLayout::admitWindowLeft(int workspace) {
    auto s = getSliderForWorkspace(workspace);
    if (s == nullptr) {
        slidr_log(INFO, "No slider on workspace {}", workspace);
        return;
    }
    s->admitWindowLeft();
}

void SlidrLayout::expelWindowRight(int workspace) {
    auto s = getSliderForWorkspace(workspace);
    if (s == nullptr) {
        slidr_log(INFO, "No slider on workspace {}", workspace);
        return;
    }
    s->expelWindowRight();
}

void SlidrLayout::printLayout() {
    using namespace std;

    for (auto& s : sliders) {
        dbgPrintSlider(*s);
        cout << endl;
    }
}

Slider* SlidrLayout::getSliderForWorkspace(int workspace) {
    for (auto& s : sliders) {
        if (s->hasWorkspace(workspace))
            return s.get();
    }
    return nullptr;
}

Slider* SlidrLayout::getSliderForWindow(PHLWINDOW window) {
    for (auto& s : sliders) {
        if (s->hasWindow(window))
            return s.get();
    }
    return nullptr;
}

Vector2D SlidrLayout::predictSizeForNewWindowTiled() {
    return {};
}
