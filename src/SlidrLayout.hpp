#pragma once

#include <hyprland/src/layout/IHyprLayout.hpp>

#include "Slider.hpp"

class SlidrLayout : public IHyprLayout {
  public:
    virtual void onWindowCreatedTiling(PHLWINDOW, eDirection = DIRECTION_DEFAULT);
    virtual void onWindowRemovedTiling(PHLWINDOW);
    virtual void onWindowFocusChange(PHLWINDOW);
    virtual bool isWindowTiled(PHLWINDOW);
    virtual void recalculateMonitor(const int& monitor_id);
    virtual void recalculateWindow(PHLWINDOW);
    virtual void resizeActiveWindow(const Vector2D&, eRectCorner, PHLWINDOW);
    virtual void fullscreenRequestForWindow(PHLWINDOW, eFullscreenMode, bool);
    virtual std::any layoutMessage(SLayoutMessageHeader, std::string);
    virtual SWindowRenderLayoutHints requestRenderHints(PHLWINDOW);
    virtual void switchWindows(PHLWINDOW, PHLWINDOW);
    virtual void moveWindowTo(PHLWINDOW, const std::string&, bool); // TODO what's the bool?
    virtual void alterSplitRatio(PHLWINDOW, float, bool);
    virtual std::string getLayoutName();
    virtual PHLWINDOW getNextWindowCandidate(PHLWINDOW);
    virtual void replaceWindowDataWith(PHLWINDOW, PHLWINDOW);
    virtual Vector2D predictSizeForNewWindowTiled();

    virtual void onEnable();
    virtual void onDisable();

    void cycleWindowSize(int workspace);
    void moveFocus(int workspace, Direction);
    void alignWindow(int workspace, Direction);
    void admitWindowLeft(int workspace);
    void expelWindowRight(int workspace);
    void printLayout();

  private:
    Slider* getSliderForWorkspace(int);
    Slider* getSliderForWindow(PHLWINDOW);

    std::list<std::unique_ptr<Slider>> sliders;
};
