#pragma once

#include <hyprland/src/helpers/Vector2D.hpp>
#include <optional>
#include <list>

#include <hyprland/src/desktop/Window.hpp>

enum class StackWidth {
    OneThird,
    OneHalf,
    TwoThirds
};

enum class Direction {
    Left,
    Right,
    Up,
    Down,
    Center
};

struct SBox {
    SBox() : x(0), y(0), w(0), h(0) {}
    SBox(double x_, double y_, double w_, double h_) : x(x_), y(y_), w(w_), h(h_) {}
    SBox(Vector2D pos, Vector2D size) : x(pos.x), y(pos.y), w(size.x), h(size.y) {}

    void setSize(double w_, double h_) {
        w = w_;
        h = h_;
    }
    void move(double dx, double dy) {
        x += dx;
        y += dy;
    }
    void setPos(double x_, double y_) {
        x = x_;
        y = y_;
    }

    double x, y, w, h;
};

struct WinStack {
    WinStack(PHLWINDOW);

    bool isActive(PHLWINDOW);
    bool hasWindow(PHLWINDOW);
    bool empty();
    size_t size();
    void removeWindow(PHLWINDOW);
    void focusWindow(PHLWINDOW);
    bool toggleFullscreenActiveWindow();
    bool isFullscreenActiveWindow();
    PHLWINDOW getActiveWindow();
    void admitWindow(PHLWINDOW);
    PHLWINDOW expelActive();
    void moveFocusUp();
    void moveFocusDown();
    void moveActiveUp();
    void moveActiveDown();
    void applyGeom();

    SBox geom;
    StackWidth width;
    int act_idx;
    bool act_maxd;
    std::vector<PHLWINDOW> stack;
};

class Slider {
  public:
    Slider(int workspace_, PHLWINDOW w_, SBox fullsz_, SBox maxsz_) : workspace(workspace_), fullsz(fullsz_), maxsz(maxsz_) {
        addActiveWindow(w_);
    }

    bool isActive(PHLWINDOW);
    bool hasWindow(PHLWINDOW);
    bool hasWorkspace(int);
    void addActiveWindow(PHLWINDOW);
    void removeWindow(PHLWINDOW);
    void focusWindow(PHLWINDOW);
    void resizeActiveStack();
    void centerActiveStack();
    void toggleMaximizeActiveStack();
    void toggleFullscreenActiveWindow();
    void ensureFullscreenActiveWindow();
    void moveActiveStack(Direction);
    void moveFocus(Direction);
    void alignStick(Direction);
    void admitWindowLeft();
    void expelWindowRight();
    void recalculateSizePos();

    PHLWINDOW getActiveWindow();
    void setSizes(SBox fullsz_, SBox maxsz_) {
        fullsz = fullsz_;
        maxsz = maxsz_;
    }

  private:
    void moveFocusLeft();
    void moveFocusRight();

    int workspace;
    SBox fullsz, maxsz;
    std::optional<WinStack> active;
    std::list<WinStack> lefts;
    std::list<WinStack> rights;

    friend struct std::formatter<Slider>;
    friend void dbgPrintSlider(Slider& s);
};

template <>
struct std::formatter<StackWidth> {
    constexpr auto parse(std::format_parse_context& ctx) {
        return ctx.begin();
    }

    auto format(const StackWidth& sw, std::format_context& ctx) const {
        switch (sw) {
            case StackWidth::OneThird: return std::format_to(ctx.out(), "[StackWidth: {}]", "one-third");
            case StackWidth::OneHalf: return std::format_to(ctx.out(), "[StackWidth: {}]", "one-half");
            case StackWidth::TwoThirds: return std::format_to(ctx.out(), "[StackWidth: {}]", "two-thirds");
        }
        return std::format_to(ctx.out(), "{}", "[StackWidth: bad value]");
    }
};

template <>
struct std::formatter<Direction> {
    constexpr auto parse(std::format_parse_context& ctx) {
        return ctx.begin();
    }

    auto format(const Direction& dir, std::format_context& ctx) const {
        switch (dir) {
            case Direction::Left: return std::format_to(ctx.out(), "Dir:{}", "left");
            case Direction::Right: return std::format_to(ctx.out(), "Dir:{}", "right");
            case Direction::Center: return std::format_to(ctx.out(), "Dir:{}", "center");
        }
        return std::format_to(ctx.out(), "{}", "[SWindowSize: bad value]");
    }
};

template <>
struct std::formatter<SBox> {
    constexpr auto parse(std::format_parse_context& ctx) {
        return ctx.begin();
    }

    auto format(const SBox& b, std::format_context& ctx) const {
        return std::format_to(ctx.out(), "[Sbox: x:{} y:{} w:{} h:{}]", b.x, b.y, b.w, b.h);
    }
};

template <>
struct std::formatter<WinStack> {
    constexpr auto parse(std::format_parse_context& ctx) {
        return ctx.begin();
    }

    auto format(const WinStack& ws, std::format_context& ctx) const {
        return std::format_to(ctx.out(), "[WinStack: size:{} idx:{} width:{} geom:{}]", ws.stack.size(), ws.act_idx, ws.width, ws.geom);
    }
};

template <>
struct std::formatter<Slider> {
    constexpr auto parse(std::format_parse_context& ctx) {
        return ctx.begin();
    }

    auto format(const Slider& s, std::format_context& ctx) const {
        return std::format_to(ctx.out(), "[Slider: active:a lefts:{} rights:{}]", s.lefts.size(), s.rights.size());
    }
};

void dbgPrintSlider(Slider& s);
