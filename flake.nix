{
  description = "Hyprslidr plugin for Hyprland";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    hyprland.url = "git+https://github.com/hyprwm/Hyprland?submodules=1";
    # hyprland = {
    #   type = "git";
    #   url = "https://github.com/hyprwm/Hyprland";
    #   submodules = true;
    # };
    # hyprland.url = "git+https://github.com/hyprwm/Hyprland?submodules=1&rev=cba1ade848feac44b2eda677503900639581c3f4";

    hyprwayland-scanner.url = "github:hyprwm/hyprwayland-scanner";
  };

  outputs = { self, nixpkgs, flake-utils, hyprland, hyprwayland-scanner, ... } @ inputs:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
        };

        hyprlandPkgs = hyprland.packages.${system};

        # Common set of dependencies
        commonDeps = with pkgs; [
          cairo
          eglexternalplatform
          egl-wayland
          pkg-config
	  hyprwayland-scanner.packages.${pkgs.system}.default
	  hyprland.packages.${pkgs.system}.hyprland
          hyprlang
	  libdrm
          libinput
          libGL
          libxkbcommon
          pixman
          wayland
	  wayland-scanner
	  wayland-protocols
	  wlroots
          xorg.libxcb
          xorg.xcbutil
          xorg.xcbutilwm
          pixman
	  libglvnd
	];

	in
	{
          # Define devShells using common dependencies
          devShells.default = pkgs.mkShell {
            buildInputs = commonDeps;
          };

          # Define packages using the same common dependencies
          packages = {
            hyprslidr = pkgs.stdenv.mkDerivation {
              pname = "hyprslidr";
              version = "1.0";

              src = ./.;

              buildInputs = commonDeps;

              buildPhase = ''
              make
              '';

              installPhase = ''
              # mkdir -p $out/hypr/plugins
              # cp hyprslidr.so $out/hypr/plugins/
mkdir -p $out/lib
cp hyprslidr.so $out/lib/libhyprslidr.so
            '';

              meta = with pkgs.lib; {
		description = "Hyprslidr plugin for Hyprland";
		homepage = "https://gitlab.com/magus/hyprslidr/";
		license = licenses.bsd3;
		platforms = platforms.linux;
		postInstallMessage = ''
                To complete the installation of hyprslidr, create a symlink in your Hyprland configuration directory:

                   ln -s $(nix path-info -f . hyprslidr)/hypr/plugins/hyprslidr.so $XDG_CONFIG_HOME/hypr/plugins/hyprslidr.so

                Replace /path/to/nix/store/...-hyprslidr-1.0 with the actual path to the built package.
                '';
              };
            };
          };

          # Define defaultPackage
          defaultPackage = self.packages.${system}.hyprslidr;
	}
    );
}
