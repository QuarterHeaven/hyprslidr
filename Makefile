.PHONY: all clean install dev

SRCS = src/main.cpp \
	src/dispatchers.cpp \
	src/SlidrLayout.cpp \
	src/Slider.cpp
OBJS = $(SRCS:%.cpp=%.o)

%.o : %.cpp
	$(CXX) -DWLR_USE_UNSTABLE -fPIC -fno-gnu-unique \
	    -I /usr/include/hyprland/wlroots \
	    -MT $@ -MMD -MP -MF $*.d \
	    -g `pkg-config --cflags pixman-1 libdrm hyprland` -std=c++2b \
	    -c $< -o $@

all: hyprslidr.so

hyprslidr.so: $(OBJS)
	$(CXX) -shared -fPIC --no-gnu-unique \
	    -DWLR_USE_UNSTABLE \
	    -g `pkg-config --cflags pixman-1 libdrm hyprland` -std=c++2b \
	    $(OBJS) \
	    -o hyprslidr.so

clean:
	rm -f $(OBJS)
	rm -f $(SRCS:%.cpp=%.d)
	rm -f ./hyprslidr.so
	rm -f compile_commands.json

install: hyprslidr.so
	mkdir -p `xdg-user-dir`/.config/hypr/plugins
	cp $< `xdg-user-dir`/.config/hypr/plugins

dev: clean compile_commands.json

compile_commands.json: Makefile $(SRCS)
	bear -- make hyprslidr.so

-include $(SRCS:%.cpp=%.d)
